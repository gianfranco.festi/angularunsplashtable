import {HttpClient} from '@angular/common/http';
import {Component, ViewChild, AfterViewInit} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {merge, Observable, of as observableOf} from 'rxjs';
import {catchError, map, startWith, switchMap} from 'rxjs/operators';

/**
 * @title Table retrieving data through HTTP
 * https://unsplash.com/documentation#search-photos
 */
@Component({
  selector: 'table-http-example',
  styleUrls: ['table-http-example.css'],
  templateUrl: 'table-http-example.html',
})
export class TableHttpExample implements AfterViewInit {
  displayedColumns: string[] = ['id','created_at','description','regular'];
  exampleDatabase: ExampleHttpDatabase | null;
  data: Photo[] = [];
  keySearch:string="office";
  resultsLength = 0;
  isLoadingResults = true;
  isRateLimitReached = false;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private _httpClient: HttpClient) {}
  applyFilter(txt:string){
    // this.exampleDatabase!.getRepoIssues(
    //   this.sort.active, this.sort.direction, this.paginator.pageIndex,this.paginator.pageSize,txt)
    //   .subscribe(data => this.data=data.results);
    this.update(txt);

  }

  update(keySearch:string) {
    this.keySearch=keySearch;
    this.exampleDatabase = new ExampleHttpDatabase(this._httpClient);

    // If the user changes the sort order, reset back to the first page.
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    merge(this.sort.sortChange, this.paginator.page,this.keySearch)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.exampleDatabase!.getRepoIssues(
            this.sort.active, this.sort.direction, this.paginator.pageIndex,this.paginator.pageSize,this.keySearch);
        }),
        map(data => {
          // Flip flag to show that loading has finished.
          this.isLoadingResults = false;
          this.isRateLimitReached = false;
          this.resultsLength = data.total_pages;

          return data.results;
        }),
        catchError(() => {
          this.isLoadingResults = false;
          // Catch if the GitHub API has reached its rate limit. Return empty data.
          this.isRateLimitReached = true;
          return observableOf([]);
        })
      ).subscribe(data => {
        this.data = data;
        console.log(data);

        }
        );
  }
  ngAfterViewInit() {
    this.update(this.keySearch);
   }
}

export interface UnsplashResult {
  total: number;
  total_pages: number,
  results: Photo[];
}



export interface Photo {
  id  : string;
  created_at : Date;
  width: number;
  height:number;
  color: string;

  description: string;
  urls:{
    raw: string;
    full: string;
    regular: string;
    small: string;
    thumb : string;
  }
}

/** An example database that the data source uses to retrieve data for the table. */
export class ExampleHttpDatabase {
  constructor(private _httpClient: HttpClient) {}

  getRepoIssues(sort: string, order: string, page: number,pagesize:number,keySearch:string): Observable<UnsplashResult> {
    console.log(order);
    order = order ==="desc" ? "latest" : "relevant";
    console.log(order);

    const href ="https://api.unsplash.com/search/photos";
    const requestUrl =
         `${href}?page=${page + 1}&query=${keySearch}&per_page=${pagesize}&order_by=${order};`
    return this._httpClient.get<UnsplashResult>(requestUrl,
      {
        headers: {
          Authorization:
            'Client-ID 6g4hstyK1ZH2aTFzxTeVceIj_dzrCFIeszGrEO-BjwM'
        }
      });
  }
}



